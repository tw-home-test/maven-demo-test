FROM tomcat:8
ADD target/hello-world.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]
